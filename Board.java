public class Board{
	private Die a;
	private Die b;
	private boolean[] closedTiles;
	
	public Board(){
		this.a = new Die();
		this.b = new Die();
		this.closedTiles = new boolean[12];
	}
	
	public String toString(){
		String value = "";
		int i = 0;
		for(boolean tile : this.closedTiles){
			if(tile == false){
				value = value + ((i++)+1);
			}
			else{
				value = value + "X";
			}
		}
		return value;
	}
	
	public boolean playATurn(){
		this.a.roll(a.getRandom());
		this.b.roll(b.getRandom());
		System.out.println(this.a.roll(a.getRandom()));
		System.out.println(this.b.roll(b.getRandom()));
		int pipSum = (this.a.roll(a.getRandom())) + (this.b.roll(b.getRandom()));
		if(this.closedTiles[pipSum-1] == false){
			this.closedTiles[pipSum-1] = true;
			System.out.println("Closing tile: "+pipSum);
			return false;
		}
		else{
			System.out.println("The tile at this position is already shut");
			return true;
		}
	}
}