import java.util.Random;
public class Die{
	private int pips;
	private Random random;
	
	//contructor
	public Die(){
		this.pips = 6;
		this.random = new Random();
	}
	
	public int getPips(){
		return this.pips;
	}
	
	public Random getRandom(){
		return this.random;
	}
	
	public int roll(Random random){
		return random.nextInt((this.pips)+1);
	}
	
	public String toString(){
		return "The value of pips is: "+this.pips;
	}
}